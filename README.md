gesthiper
===============

Projecto de gestão de produtos de hipermercados para a cadeira de Laboratórios de
Informática III, do curso de Licenciatura em Engenharia Informática da Universidade
do Minho. Escrito em C.

Autor: André Santos (a61778)

####Contacto Email:
a61778[at]alunos.uminho.pt

Como usar o programa:
----------------------

1) Compilar o código fonte:  
* *make*  

2) Correr o programa de uma destas formas:  
* *make run*  -  Corre o programa com ficheiro Compras.txt  
* *make run1m* - Corre o programa com ficheiro Compras1.txt  
* *make run3m* - Corre o programa com ficheiro Compras3.txt  
* *./gesthiper*  - Mesmo que *make run*  
* *./gesthiper <\Ficheiro_clientes> <\Ficheiro_produtos> <\Ficheiro_compras>*  - Corre programa com ficheiros especificados como argumento



